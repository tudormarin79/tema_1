function distance(first, second) {
	//TODO: implementați funcția
	// TODO: implement the function

	for (var i = 0; i < first.length; i++) {
		if (!isNaN(first[i]))
			return 2;
	}

	for (var i = 0; i < second.length; i++) {
		if (!isNaN(second[i]))
			return 2;
	}

	if (!Array.isArray(first) || !Array.isArray(second)) {
		throw new Error('InvalidType')
	}

	if (Array.isArray(first) && first.length <= 0) {
		return 0;
	}
	if (Array.isArray(second) && second.length <= 0) {
		return 0;
	}


	var seenFirst = {};
	var outFirst = [];
	var j = 0;

	for (var i = 0; i < first.length; i++) {
		var item = first[i];
		if (seenFirst[item] !== 1) {
			seenFirst[item] = 1;
			outFirst[j++] = item;
		}
	}

	var seenSecond = {};
	var outSecond = [];
	j = 0;

	for (var i = 0; i < second.length; i++) {
		var item = second[i];
		if (seenSecond[item] !== 1) {
			seenSecond[item] = 1;
			outSecond[j++] = item;
		}
	}

	var count = 0;
	var flag = false;


	if (outSecond.length > outFirst.length) {

		var a = outSecond;
		var b = outFirst;

	} else {

		var a = outFirst;
		var b = outSecond;

	}
	for (var i = 0; i < a.length; i++) {
		flag = false;
		for (var j = 0; j < b.length; j++) {
			if (a[i] == b[j]) {
				flag = true;
			}
		}

		if (flag == false) {
			count++;
		}
	}

	return count;

}


module.exports.distance = distance